package com.like.gulimall.product.dao;

import com.like.gulimall.product.entity.SkuImagesEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * sku图片
 * 
 * @author like
 * @email lk583552696@163.com
 * @date 2021-11-27 16:48:07
 */
@Mapper
public interface SkuImagesDao extends BaseMapper<SkuImagesEntity> {
	
}
