package com.like.gulimall.order.dao;

import com.like.gulimall.order.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author like
 * @email lk583552696@163.com
 * @date 2022-02-14 13:22:39
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
