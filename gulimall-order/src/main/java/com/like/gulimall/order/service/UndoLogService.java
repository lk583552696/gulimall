package com.like.gulimall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.like.common.utils.PageUtils;
import com.like.gulimall.order.entity.UndoLogEntity;

import java.util.Map;

/**
 * 
 *
 * @author like
 * @email lk583552696@163.com
 * @date 2022-02-14 13:22:39
 */
public interface UndoLogService extends IService<UndoLogEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

