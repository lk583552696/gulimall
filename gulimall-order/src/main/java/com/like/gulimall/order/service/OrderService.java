package com.like.gulimall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.like.common.utils.PageUtils;
import com.like.gulimall.order.entity.OrderEntity;

import java.util.Map;

/**
 * 订单
 *
 * @author like
 * @email lk583552696@163.com
 * @date 2022-02-14 13:22:39
 */
public interface OrderService extends IService<OrderEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

