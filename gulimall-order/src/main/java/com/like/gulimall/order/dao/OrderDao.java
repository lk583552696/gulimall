package com.like.gulimall.order.dao;

import com.like.gulimall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author like
 * @email lk583552696@163.com
 * @date 2022-02-14 13:22:39
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
