package com.like.gulimall.coupon.dao;

import com.like.gulimall.coupon.entity.CouponEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券信息
 * 
 * @author like
 * @email lk583552696@163.com
 * @date 2022-02-14 11:04:58
 */
@Mapper
public interface CouponDao extends BaseMapper<CouponEntity> {
	
}
