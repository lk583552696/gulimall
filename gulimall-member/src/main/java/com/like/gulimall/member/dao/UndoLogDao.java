package com.like.gulimall.member.dao;

import com.like.gulimall.member.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author like
 * @email lk583552696@163.com
 * @date 2022-02-14 12:43:19
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
