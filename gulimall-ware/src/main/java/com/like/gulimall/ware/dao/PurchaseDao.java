package com.like.gulimall.ware.dao;

import com.like.gulimall.ware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author like
 * @email lk583552696@163.com
 * @date 2022-02-14 13:29:37
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {
	
}
